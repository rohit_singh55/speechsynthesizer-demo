//
//  main.m
//  SpeechSynthesizerDemo
//
//  Created by Rohit Singh on 09/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSAppDelegate class]));
    }
}
