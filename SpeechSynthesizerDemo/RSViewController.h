//
//  RSViewController.h
//  SpeechSynthesizerDemo
//
//  Created by Rohit Singh on 09/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface RSViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *tyedTextField;
@property (strong, nonatomic) IBOutlet UISlider *rateSlider;
@property(strong,nonatomic) AVSpeechSynthesizer *synthesizer;
@property (strong, nonatomic) IBOutlet UISlider *pitchSlider;
- (IBAction)speak:(id)sender;
@end
