//
//  RSAppDelegate.h
//  SpeechSynthesizerDemo
//
//  Created by Rohit Singh on 09/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
