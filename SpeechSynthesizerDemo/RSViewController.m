//
//  RSViewController.m
//  SpeechSynthesizerDemo
//
//  Created by Rohit Singh on 09/09/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "RSViewController.h"

@interface RSViewController ()

@end

@implementation RSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.synthesizer=[[AVSpeechSynthesizer alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)speak:(id)sender {
    AVSpeechUtterance *utterance=[[AVSpeechUtterance alloc]initWithString:self.tyedTextField.text];
    utterance.rate=[self.rateSlider value];
    utterance.pitchMultiplier=[self.pitchSlider value];
    [self.synthesizer speakUtterance:utterance];
    
}
@end
